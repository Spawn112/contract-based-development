﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgaver
{
    public static class W10_opgaver
    {
        public static bool element_i_G12(int n)
        {
            var g12 = new List<int>();
            for (int i = 1; i <= 100; i++)
            {
                if (i % 12 == 0)
                    g12.Add(i);
            }
            return g12.Contains(n);
        }

        public static List<List<char>> GetCollection()
        {
            var coll = new List<List<char>>();
            var total = 89;
            var numberOfA = 55;
            var numberOfB = 24;
            var numberOfC = 47;
            var numberOfAAndB = 7;
            var numberOfBAndC = 4;
            var numberOfAll = 1;
            while(numberOfAll > 0)
            {
                coll.Add(new List<char>() { 'A', 'B', 'C' });
                total--;
                numberOfA--;
                numberOfB--;
                numberOfC--;
                numberOfAAndB--;
                numberOfBAndC--;
                numberOfAll--;
            }
            while( numberOfAAndB > 0)
            {
                coll.Add(new List<char>() { 'A', 'B' });
                total--;
                numberOfA--;
                numberOfB--;
                numberOfAAndB--;
            }
            while (numberOfBAndC > 0)
            {
                coll.Add(new List<char>() { 'B', 'C' });
                total--;
                numberOfB--;
                numberOfC--;
                numberOfBAndC--;
            }
            while (numberOfB > 0)
            {
                coll.Add(new List<char>() { 'B' });
                total--;
                numberOfB--;
            }
            var numberOfAAndC = (numberOfA + numberOfC) - total;
            while (numberOfAAndC > 0)
            {
                coll.Add(new List<char>() { 'A', 'C' });
                total--;
                numberOfA--;
                numberOfC--;
                numberOfAAndC--;
            }
            while (numberOfA > 0 && (total - numberOfC > 0))
            {
                coll.Add(new List<char>() { 'A' });
                total--;
                numberOfA--;
            }
            while (numberOfC > 0)
            {
                coll.Add(new List<char>() { 'C' });
                total--;
                numberOfC--;
            }
            if(total != 0)
            {
                throw new Exception("total is:" + total + " while it should be 0.");
            }
            return coll;
        }
    }
}