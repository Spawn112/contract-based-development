﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgaver
{
    public class W12_opgaver
    {
        public int Middle(int x, int y, int z)
        {
            var list = new List<int>() { x, y, z };
            list.Sort();
            return list[1];
        }

        public void AssertNumberEqualsAverage(double x, int[] t)
        {
            Debug.Assert(x == t.Sum() / t.Count());
        }

        public int Median(int[] b)
        {
            throw new NotImplementedException();
        }

        public void Asserts()
        {
            int[] b = null;
            Debug.Assert(Median(b) == b[b.Length / 2]);
        }
    }
}