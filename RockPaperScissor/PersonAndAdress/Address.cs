﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonAndAdress
{
    public class Address
    {
        public string _street { get; set; }
        public int _zip { get; set; }
        public string _city { get; set; }
    }
}