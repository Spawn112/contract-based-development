﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonAndAdress
{
    public class Person
    {
        public Address _address { get; set; }
        public DateTime _dob { get; set; }
        public double _weight { get; set; }

        public Person DClone()
        {
            var person = new Person();
            person._address = new Address();
            person._address._city = _address._city;
            person._address._street = _address._street;
            person._address._zip = _address._zip;
            person._dob = new DateTime(_dob.Ticks);
            person._weight = _weight;
            return person;
        }
    }
}