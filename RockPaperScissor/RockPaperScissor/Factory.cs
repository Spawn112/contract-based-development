﻿using RockPaperScissor.Controllers;
using RockPaperScissor.Entities;
using RockPaperScissor.Interfaces;
using System.Collections.Generic;

namespace RockPaperScissor
{
    public class Factory
    {
        /*
         * @Ensure Result != null
         */
        public GameController NewGameController()
        {
            return new GameController();
        }

        /*
         * @Ensure Result != null
         */
        public RuleSetController NewRuleSetController()
        {
            return new RuleSetController();
        }

        /*
         * @Require string.IsNullOrEmpty(name) == false
         * @Ensure Result != null
         */
        public IPlayer NewAIPlayer(string name)
        {
            return new AI(name);
        }

        /*
         * @Require string.IsNullOrEmpty(name) == false
         * @Ensure Result != null
         */
        public IPlayer NewHumanPlayer(string name)
        {
            return new Human(name);
        }

        /*
         * @Require players != null
         * @Require players.Count >= 1
         * @Require rules != null
         * @Ensure Return != null
         * @Ensure Return.GetPlayers() != null
         * @Ensure Return.GetPlayers().Count == player.Count
         */
        public IGame NewGame(List<IPlayer> players, RuleSet rules)
        {
            return new RockPaperScissorGame(players, rules);
        }

        /*
         * @Require game != null
         * @Ensure Return != null
         * @Ensure Return.game != null
         * @Ensure Return.game == game
         */
        public ITournament NewTournament(IGame game)
        {
            return new Tournament(game);
        }
    }
}