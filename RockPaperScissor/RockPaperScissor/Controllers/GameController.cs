﻿using RockPaperScissor.Entities;
using RockPaperScissor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace RockPaperScissor.Controllers
{
    public class GameController
    {
        public Factory Factory
        {
            get
            {
                return new Factory();
            }
        }

        /*
         * @Require playerNames != null
         * @Require playerNames.Count >= 1
         * @Ensure return != null
         * @Ensure return.count = playernames.count
         */
        public List<IPlayer> GetPlayers(Dictionary<string, bool> playerNames)
        {
            var players = new List<IPlayer>();
            foreach (var playerName in playerNames.Keys)
            {
                var isAI = playerNames[playerName];
                if (isAI)
                {
                    players.Add(Factory.NewAIPlayer(playerName));
                }
                else
                {
                    players.Add(Factory.NewHumanPlayer(playerName));
                }
            }
            return players;
        }

        /*
         * @Require game != null
         * @Ensure game.numberofmatchesplayed = game.numberofmatchesplayed@pre + 1
         */
        public void PlayMatch(IGame game)
        {
            var players = game.GetPlayers();
            for (int i = 0; i < players.Count - 1; i++)
            {
                for (int j = i + 1; j < players.Count; j++)
                {
                    SetPlayerAttacks(players[i], players[j]);
                    SetWinner(players[i], players[j], game);
                }
            }
            game.NumberOfMatchesPlayed++;
        }

        /*
         * @Require player1 != null
         * @Require player2 != null
         * @Ensure player1.GetAttack() != null
         * @Ensure player2.GetAttack() != null
         */
        private void SetPlayerAttacks(IPlayer player1, IPlayer player2)
        {
            var correctAnswers = new char[] { '1', '2', '3' };
            
            player1.SetAttack((Constants.Attack)int.Parse(GetAttack(player1, correctAnswers)));
            player2.SetAttack((Constants.Attack)int.Parse(GetAttack(player2, correctAnswers)));
        }

        /*
         * @Require player != null
         * @Require correctanswers != null
         * Postconditions:
         * @Ensure player.GetAttack() != null
         */
        private string GetAttack(IPlayer player, char[] correctAnswers)
        {
            if (player is AI)
            {
                return GetComputerAttack();
            }
            else
            {
                return GetHumanPlayerAttack(player, correctAnswers);
            }
        }

        /*
         * @Ensure string.IsNullOrEmpty(return) == false
         */
        private string GetComputerAttack()
        {
            Thread.Sleep(DateTime.Now.Millisecond % 70);
            return (Math.Round(new Random().NextDouble() * 2) + 1).ToString();
        }

        /*
         * @Require player != null
         * @Require correctanswers != null
         * @Ensure string.IsNullOrEmpty(return) == false
         */
        private static string GetHumanPlayerAttack(IPlayer player, char[] correctAnswers)
        {
            var playerAnswer = '0';
            var isFirstAttempt = true;
            while (correctAnswers.Contains(playerAnswer) == false)
            {
                if (isFirstAttempt == false)
                {
                    Console.WriteLine(player.GetName() + ", you've given a wrong input, try again!");
                }
                Console.WriteLine(player.GetName() + " choose Rock[1], Paper[2] or Scissor[3]:");
                var input = Console.ReadKey(true);
                playerAnswer = input.KeyChar;
                isFirstAttempt = false;
            }
            return playerAnswer.ToString();
        }

        /*
         * @Require player1 != null
         * @Require player2 != null
         * @Require game != null
         */
        private static void SetWinner(IPlayer player1, IPlayer player2, IGame game)
        {
            IPlayer winner;
            if (player1.GetAttack() == player2.GetAttack())
            {
                Console.WriteLine("It was a draw!");
                return;
            }
            var winningCombinations = game.GetRules().WinningCombination;
            if (winningCombinations[player1.GetAttack()].Contains(player2.GetAttack()))
            {
                winner = player1;
            }
            else
            {
                winner = player2;
            }
            Console.WriteLine(winner.GetName() + " won the round!");
            game.AddScoreToPlayer(winner);
            Console.WriteLine(player1.GetName() + " has won " + player1.GetScore() + " game(s) and " + player2.GetName() + " has won " + player2.GetScore() + " game(s).");
        }
    }
}