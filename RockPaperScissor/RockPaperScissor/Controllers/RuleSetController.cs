﻿using RockPaperScissor.Entities;
using System.Collections.Generic;

namespace RockPaperScissor.Controllers
{
    public class RuleSetController
    {
        /*
         * @Require numberOfMatches > 0
         */
        public RuleSet GetDefaultRulesPVP(int numberOfMatches, bool continueIfDrawAtEnd = true)
        {
            return GetDefaultRules(numberOfMatches, 2, 0, continueIfDrawAtEnd);
        }

        /*
         * @Require numberOfMatches > 0
         */
        public RuleSet GetDefaultRulesPVAI(int numberOfMatches, bool continueIfDrawAtEnd = true)
        {
            return GetDefaultRules(numberOfMatches, 1, 1, continueIfDrawAtEnd);
        }

        /*
         * @Require numberOfMatches > 0
         * @Require numberOfAIs > 0
         */
        public RuleSet GetDefaultRulesAIVAI(int numberOfMatches, int numberOfAIs, bool continueIfDrawAtEnd = true)
        {
            return GetDefaultRules(numberOfMatches, 0, numberOfAIs, continueIfDrawAtEnd);
        }

        /*
         * @Require numberOfmatches > 0
         * @Require numberOfHumanPlayers > 0
         * @Require numberOfAIPlayers > 0
         * @Ensure Ruleset.numberOfMatches == numberOfMatches
         * @Ensure Ruleset.numberOfHumanPlayers == numberOfHumanPlayers
         * @Ensure Ruleset.numberOfAIPlayers == numberOfAIPlayers
         */
        private RuleSet GetDefaultRules(int numberOfMatches, int numberOfHumanPlayers, int numberOfAIPlayers, bool continueIfDrawAtEnd = true)
        {
            return new RuleSet(GetDefaultWinningCombination(), numberOfMatches, numberOfHumanPlayers, numberOfAIPlayers, continueIfDrawAtEnd);
        }

        /*
         * @Ensure return != null
         * @Ensure return.Keys.Count >= 1
         */
        private Dictionary<Constants.Attack, List<Constants.Attack>> GetDefaultWinningCombination()
        {
            var winningCombinations = new Dictionary<Constants.Attack, List<Constants.Attack>>();
            winningCombinations.Add(Constants.Attack.Paper, new List<Constants.Attack>() { Constants.Attack.Rock });
            winningCombinations.Add(Constants.Attack.Scissor, new List<Constants.Attack>() { Constants.Attack.Paper });
            winningCombinations.Add(Constants.Attack.Rock, new List<Constants.Attack>() { Constants.Attack.Scissor });
            return winningCombinations;
        }
    }
}