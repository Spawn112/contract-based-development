﻿namespace RockPaperScissor
{
    public struct Constants
    {
        public enum Messages { Player1NotSetCorrectly, Player2NotSetCorrectly }
        public enum Attack { Rock = 1, Paper = 2, Scissor = 3}
    }
}