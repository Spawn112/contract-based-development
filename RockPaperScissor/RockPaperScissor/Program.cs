﻿using RockPaperScissor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RockPaperScissor
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new Factory();
            var rulesController = factory.NewRuleSetController();
            var gameController = factory.NewGameController();

            Console.WriteLine("Welcome to a game of Rock paper and scissors.");

            Console.WriteLine("Enter the Playstyle of your choosing:");
            Console.WriteLine("<player vs player[1]> <player vs computer[2]> or <computer vs computer[3]>");

            var playStyle = Console.ReadKey(true).KeyChar;
            var numberOfMatches = GetNumberOfMatches();
            var playerNames = new Dictionary<string, bool>();
            RuleSet rules = null;

            switch (playStyle)
            {
                case '1':
                    Console.WriteLine("Please enter the names of the two players below.");
                    playerNames.Add(GetPlayerName(1), false);
                    playerNames.Add(GetPlayerName(2), false);
                    rules = rulesController.GetDefaultRulesPVP(numberOfMatches);
                    break;
                case '2':
                    Console.WriteLine("Please enter your name below:");
                    playerNames.Add(GetPlayerName(1), false);
                    playerNames.Add("CustomAI", true);
                    rules = rulesController.GetDefaultRulesPVAI(numberOfMatches);
                    break;
                case '3':
                    var numberOfAIs = GetNumberOfAIs();
                    for (int i = 0; i < numberOfAIs; i++)
                    {
                        playerNames.Add("CustomAI" + i, true);
                    }
                    rules = rulesController.GetDefaultRulesAIVAI(numberOfMatches, numberOfAIs);
                    break;
                default:
                    break;
            }
            var players = gameController.GetPlayers(playerNames);
            var game = factory.NewGame(players, rules);
            var tournament = factory.NewTournament(game);

            while (tournament.IsOver() == false)
            {
                gameController.PlayMatch(game);
            }

            var results = tournament.GetResults();

            foreach (var player in results.Keys)
            {
                Console.WriteLine("[" + player.GetName() + "]: has won " + results[player] + "game(s).");
            }
            Console.Read();
        }

        /*
         * @Ensure Return >= 2
         * @Ensure Return <= 5
         */
        private static int GetNumberOfAIs()
        {
            Console.WriteLine("Enter the number of computers you want to play in the tournament: (2-5)");
            var correctAnswers = new char[] { '2', '3', '4', '5' };
            var numberOfPlayers = Console.ReadKey(true).KeyChar;
            while(correctAnswers.Contains(numberOfPlayers) == false)
            {
                numberOfPlayers = Console.ReadKey(true).KeyChar;
            }
            return int.Parse(numberOfPlayers.ToString());
        }

        /*
         * @Ensure string.IsNullOrEmpty(Return) == false
         */
        private static string GetPlayerName(int player)
        {
            var name = string.Empty;
            var isFirstAttempt = true;
            while (string.IsNullOrWhiteSpace(name))
            {
                if(isFirstAttempt == false)
                {
                    Console.WriteLine("You cannot enter an empty name, please try again:", ConsoleColor.Red);
                }
                Console.Write("Player " + player + ":");
                name = Console.ReadLine();
                isFirstAttempt = false;
            }
            return name;
        }

        /*
         * @Ensure return > 0
         */
        private static int GetNumberOfMatches()
        {
            int numberOfMatches;
            Console.Write("Enter the number of matches:");
            var input = Console.ReadKey().KeyChar.ToString();
            Console.WriteLine();
            while(int.TryParse(input, out numberOfMatches) == false)
            {
                Console.Write("Try again:", ConsoleColor.Red);
                input = Console.ReadKey().KeyChar.ToString();
                Console.WriteLine();
            }
            return numberOfMatches;
        }
    }
}