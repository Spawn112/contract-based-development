﻿namespace RockPaperScissor.Interfaces
{
    public interface IPlayer
    {
        /*
         * @Ensure this.GetScore() = 0
         */
        void ResetScore();

        /*
         * @Ensure this.GetScore() = this.GetScore()@pre + 1
         */
        void AddScore();

        /*
         * @Require string.IsNullOrEmpty(this.Name) == false
         * Postconditions:
         * - 
         */
        string GetName();
        
        int GetScore();

        /*
         * @Require this.attack != null
         * @Ensure this.GetAttack() == attack
         */
        void SetAttack(Constants.Attack attack);

        /*
         * @Require this.attack != null
         */
        Constants.Attack GetAttack();
    }
}