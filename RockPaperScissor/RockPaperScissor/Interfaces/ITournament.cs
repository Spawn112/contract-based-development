﻿using System.Collections.Generic;

namespace RockPaperScissor.Interfaces
{
    public interface ITournament
    {
        /*
         * @Require game != null
         * @Require game.GetPlayers() != null
         * @Require game.GetPlayers().Any()
         * @Require game.GetRules() != null
         */
        bool IsOver();

        /*
         * @Require game != null
         * @Require game.GetPlayers() != null
         * @Require game.GetPlayers().Any()
         */
        Dictionary<IPlayer, int> GetResults();
    }
}