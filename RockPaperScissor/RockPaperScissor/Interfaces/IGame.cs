﻿using RockPaperScissor.Entities;
using System.Collections.Generic;

namespace RockPaperScissor.Interfaces
{
    public interface IGame
    {
        /*
         * @Require winner != null
         * @Ensure winner.GetScore() == winner.GetScore()@pre + 1
         */
        void AddScoreToPlayer(IPlayer winner);

        /*
         * @Require this.Players.Count >= 1
         * @Ensure foreach this.Players  player.GetScore() == 0
         */
        void StartNewGame();

        /*
         * @Require this.Players.Count >= 1
         * @Require index >= 0
         * @Require index < this.Players.Count
         */
        IPlayer GetPlayer(int index);

        /*
         * @Require this.Players.Count >= 1
         * @Require string.IsNullOrEmpty(name) == false
         * @Require this.Players.Any(player => player.name == name)
         */
        IPlayer GetPlayer(string name);

        /*
         * @Require this.Players != null
         */
        List<IPlayer> GetPlayers();

        /*
         * @Require this.Rules != null
         */
        RuleSet GetRules();
        
        int NumberOfMatchesPlayed { get; set; }
    }
}