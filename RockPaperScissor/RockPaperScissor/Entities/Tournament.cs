﻿using RockPaperScissor.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace RockPaperScissor.Entities
{
    public class Tournament : ITournament
    {
        private readonly IGame game;

        public Tournament(IGame game)
        {
            this.game = game;
        }

        public Dictionary<IPlayer, int> GetResults()
        {
            var result = new Dictionary<IPlayer, int>();
            foreach (var player in game.GetPlayers().OrderByDescending(player => player.GetScore()))
            {
                result.Add(player, player.GetScore());
            }
            return result;
        }

        public bool IsOver()
        {
            if (game.GetRules().NumberOfMatches > game.NumberOfMatchesPlayed)
            {
                return false;
            }
            if(game.GetRules().ContinueIfDrawAtEnd)
            {
                return game.GetPlayers().Select(player => player.GetScore()).Distinct().Count() > 1;
            }
            return true;
        }
    }
}