﻿using RockPaperScissor.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace RockPaperScissor.Entities
{
    public class RockPaperScissorGame : IGame
    {
        private readonly List<IPlayer> players;
        private readonly RuleSet rules;

        public int NumberOfMatchesPlayed { get; set; }

        public RockPaperScissorGame(List<IPlayer> players, RuleSet rules)
        {
            this.players = players;
            this.rules = rules;
            StartNewGame();
        }

        public IPlayer GetPlayer(int index)
        {
            return players[index];
        }

        public IPlayer GetPlayer(string name)
        {
            var possiblePlayer = players.Where(player => player.GetName().ToLowerInvariant() == name.ToLowerInvariant());
            if (possiblePlayer.Any())
            {
                return possiblePlayer.First();
            }
            return null;
        }

        public List<IPlayer> GetPlayers()
        {
            return players;
        }

        public RuleSet GetRules()
        {
            return rules;
        }
        
        public void StartNewGame()
        {
            foreach (var player in players)
            {
                player.ResetScore();
            }
        }

        public void AddScoreToPlayer(IPlayer player)
        {
            player.AddScore();
        }
    }
}