﻿using System.Collections.Generic;

namespace RockPaperScissor.Entities
{
    public class RuleSet
    {
        private readonly Dictionary<Constants.Attack, List<Constants.Attack>> winningCombination;
        private readonly int numberOfMatches;
        private readonly int numberOfHumanPlayers;
        private readonly int numberOfAIPlayers;
        private readonly bool continueIfDrawAtEnd;

        public RuleSet(Dictionary<Constants.Attack, List<Constants.Attack>> winningCombination, int numberOfMatches, int numberOfHumanPlayers, int numberOfAIPlayers, bool continueIfDrawAtEnd = true)
        {
            this.winningCombination = winningCombination;
            this.numberOfMatches = numberOfMatches;
            this.numberOfHumanPlayers = numberOfHumanPlayers;
            this.numberOfAIPlayers = numberOfAIPlayers;
            this.continueIfDrawAtEnd = continueIfDrawAtEnd;
        }
        
        /*
         * @Require winningCombinations != null
         */
        public Dictionary<Constants.Attack, List<Constants.Attack>> WinningCombination { get { return winningCombination; } }
        
        public int NumberOfMatches { get { return numberOfMatches; } }
        
        public int NumberOfHumanPlayers { get { return numberOfHumanPlayers; } }
        
        public int NumberOfAIPlayers { get { return numberOfAIPlayers; } }
        
        public bool ContinueIfDrawAtEnd { get { return continueIfDrawAtEnd; } }
    }
}