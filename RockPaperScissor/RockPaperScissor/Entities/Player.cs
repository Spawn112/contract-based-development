﻿using RockPaperScissor.Interfaces;

namespace RockPaperScissor.Entities
{
    public class Player : IPlayer
    {
        private string name;
        private int gamesWon;
        private Constants.Attack chosenAttack;

        public Player() { }

        public Player(string name)
        {
            SetName(name);
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }

        public void ResetScore()
        {
            gamesWon = 0;
        }

        public void AddScore()
        {
            gamesWon++;
        }

        public int GetScore()
        {
            return gamesWon;
        }

        public void SetAttack(Constants.Attack attack)
        {
            chosenAttack = attack;
        }

        public Constants.Attack GetAttack()
        {
            return chosenAttack;
        }
    }
}